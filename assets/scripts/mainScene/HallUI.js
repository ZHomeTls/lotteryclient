/*
大厅界面
*/ 
cc.Class({
    extends: cc.Component,

    properties: {
       
        chatRoomPrefab:{
            default: null,
            type: cc.Prefab
        },

        itemPrefab:{
            default: null,
            type: cc.Prefab
        },
        outItemList:{
            default: null,
            type: cc.Node
        },
        hotItemPrefab: {
            default: null,
            type: cc.Prefab
        },
        outHotList:{
            default: null,
            type: cc.Layout
        },
     
        jackPotLab:{
            default: null,
            type: cc.Label
        },

        hotListItem:[],
        itemListItem:[],

        pvActivity:{
            default:null,
            type:cc.PageView
        },

        spBase:{
            default:null,
            type:cc.SpriteFrame
        },

        activityPrefab: {
            default: null,
            type: cc.Prefab
        },

        aniPao:cc.Animation,//跑马灯动画
        aniLabel:cc.Label,

        _paoList:[],//跑马灯
        _playNum:0
    },

    // use this for initialization
    onLoad: function () {
        this.refreshInfo();
        window.Notification.on("HallUI_toGotoRoom",function(data){
            this.toGotoRoom(data);
        },this);

        window.Notification.on('rLotState_push',function(data){
            this.refreshList(data);
        }.bind(this));

        window.Notification.on('rPao_push',function(data){
            this._paoList.push(data);
        }.bind(this));
    },

    update:function(dt)
    {
        if(this.aniPao.getAnimationState("ani_paomadeng").isPlaying)
        {
            return;
        } 

        if(this._playNum>0)
        {
            this._playNum--;
            this.aniPao.play('ani_paomadeng');
            return;
        }
        if(this._playNum == 0 && this._paoList.length>0)
        {
            var item = this._paoList.pop();
            this._playNum = item.Number;
            this.aniLabel.string = item.Content;
            this.aniPao.play('ani_paomadeng');
            this._playNum--;
            return;
        }

        if(this._paoList.length == 0)
        {
            this.aniLabel.string = "买彩票，中大奖，欢迎来到彩乐彩票。";
            this.aniPao.play('ani_paomadeng');
        }
    },

    
    /** 
    * 刷新列表
    * @method refreshList
    * @param {Object} data 
    */
    refreshList:function(data){
        var datas = data.data;
       
        var itemInfo = Utils.findObjByArry(this.itemListItem,"lotteryID",datas.LotteryCode);
        if(itemInfo != null)
        {
            var status = CL.MANAGECENTER.getLotteryByStatus(datas.State);
            itemInfo.item.getComponent('hall_lottery_item').setState(status);
            itemInfo.item.getComponent(cc.Button).clickEvents[0].customEventData.status = datas.State;
            return;
        }
        itemInfo = Utils.findObjByArry(this.hotListItem,"lotteryID",datas.LotteryCode);
        if(itemInfo != null)
        {
            var status = CL.MANAGECENTER.getHotLotteryByStatus(datas.State);
            itemInfo.item.getComponent('hall_lotteryHot_item').setState(status);
            itemInfo.item.getComponent(cc.Button).clickEvents[0].customEventData.status = datas.State;
            return;
        }
    },

    loadImg:function(){
        if(this.page !=null && this.page.length>0)
        {
            this.conNode = new cc.Node("page");
            this.sp = this.conNode.addComponent(cc.Sprite);
            var bt = this.conNode.addComponent(cc.Button);
    
            var clickEvents = new cc.Component.EventHandler();
            clickEvents.target = this;
            clickEvents.component = "HallUI";
            clickEvents.handler = "clickCallback";
            clickEvents.customEventData = this.page[this.imgNums].page;
            bt.clickEvents[0] = clickEvents;
            this.pvActivity.addPage(this.conNode);
    
            //动态加载图片
            var url = this.page[this.imgNums].ad;
            //cc.log("init page:"+url);
            if(url != "" && url != null &&typeof(url) != "undefined"){
                //cc.log("init page: logo2:"+url);
                cc.loader.load({url, type:"jpg"}, function(err, tex){
                    //cc.log("init page: logo3:"+tex);
                    if(tex != null){
                        this.imgNums++;
                        var sf = new cc.SpriteFrame(tex);
                        this.sp.spriteFrame = sf;
                        this.sp.node.width = 1020;
                        this.sp.node.height = 225;
                    }
                    else
                    {
                        this.imgNums++;
                        this.sp.spriteFrame = this.spBase;
                        this.sp.node.width = 1020;
                        this.sp.node.height = 225;
                    }
                    if(this.imgNums < this.page.length)
                        this.loadImg();
                }.bind(this));
            }
            else
            {
                this.imgNums++;
                this.sp.spriteFrame = this.spBase;
                this.sp.node.width = 1020;
                this.sp.node.height = 225;
                if(this.imgNums < this.page.length)
                    this.loadImg();
            }
        }
        else
        {
            this.conNode = new cc.Node("page");
            this.sp = this.conNode.addComponent(cc.Sprite);
            var bt = this.conNode.addComponent(cc.Button);
            this.sp.spriteFrame = this.spBase;
            this.sp.node.width = 1020;
            this.sp.node.height = 225;
            this.pvActivity.addPage(this.conNode);
        }
       
    },

    clickCallback:function(event,customEventData){
        if(customEventData != null && customEventData != undefined)
        {
            var canvas = cc.find("Canvas");
            var activityPage = cc.instantiate(this.activityPrefab);
            activityPage.getComponent(activityPage.name).init(customEventData);
            canvas.addChild(activityPage); 
        }
    },

    //加载初始化数据
    refreshInfo:function(){
        var self = this;
        CL.MANAGECENTER.getHallList(function(data){
            self.initHallList(data);
        });
    },

    /** 
    * 大厅列表
    * @method initHallList
    * @param {Object} data 
    */
    initHallList:function(data){
        var self = this;
        this.jackPotLab.string =  data.SystemTotalWin;
        var hotNum = 0;
        var obj = data.LotteryData;

        this.page = data.ActivityData;
        this.imgNums = 0;
        this.loadImg();
        for(var i in obj){
            var strLottery = obj[i].LotteryCode.toString();

            if(strLottery == "40010" || strLottery == "40020")
            {
                continue;
            }
            if(obj[i].IsHot && hotNum < 3)
            {
                var item1 = cc.instantiate(this.hotItemPrefab);
                this.outHotList.node.addChild(item1);

                item1.getComponent('hall_lotteryHot_item').init({
                    LotteryStatusFrame: CL.MANAGECENTER.getHotLotteryByStatus(obj[i].LotteryStatus),
                    LotteryBgFrame: CL.MANAGECENTER.getLotteryHotById(strLottery),
                });
                var checkEventHandler = new cc.Component.EventHandler();
                checkEventHandler.target = this.node; 
                checkEventHandler.component = "HallUI";
                checkEventHandler.handler = "onItemBtn";
                checkEventHandler.customEventData = {
                    lotteryCode:strLottery,
                    roomName:obj[i].LotteryName,
                    chatRoomID:obj[i].ChatRooms,
                    status:obj[i].LotteryStatus
                };
                item1.getComponent(cc.Button).clickEvents.push(checkEventHandler);
                hotNum++;

                 var data = {
                    status:obj[i].LotteryStatus,
                    item:item1,
                    lotteryID:obj[i].LotteryCode,
                    roomName:obj[i].LotteryName,
                    chatRoomID:obj[i].ChatRooms,
                };
                Chat.setChatRoomIDByLotteryID(obj[i].LotteryCode,obj[i].ChatRooms);
                this.hotListItem.push(data);
            }
            else
            {
                var item2 = cc.instantiate(this.itemPrefab);
                this.outItemList.addChild(item2);

                item2.getComponent('hall_lottery_item').init({
                    LotteryName: LotteryUtils.getLotteryTypeDecByLotteryId(strLottery),
                    LotteryStatusFrame: CL.MANAGECENTER.getLotteryByStatus(obj[i].LotteryStatus),
                    LotteryIconFrame: CL.MANAGECENTER.getLotteryIconById(strLottery),
                    Subhead: obj[i].Subhead,
                    AddawardSubhead: obj[i].AddawardSubhead,
                });
                var checkEventHandler = new cc.Component.EventHandler();
                checkEventHandler.target = this.node; 
                checkEventHandler.component = "HallUI";
                checkEventHandler.handler = "onItemBtn";
                 checkEventHandler.customEventData = {
                    lotteryCode:strLottery,
                    roomName:obj[i].LotteryName,
                    chatRoomID:obj[i].ChatRooms,
                    status:obj[i].LotteryStatus
                };
                
                item2.getComponent(cc.Button).clickEvents.push(checkEventHandler);

                var data = {
                    status:obj[i].LotteryStatus,
                    item:item2,
                    lotteryID:obj[i].LotteryCode,
                    roomName:obj[i].LotteryName,
                    chatRoomID:obj[i].ChatRooms,
                };
                Chat.setChatRoomIDByLotteryID(obj[i].LotteryCode,obj[i].ChatRooms);
                this.itemListItem.push(data);
            }
        }

    },
    
    onItemBtn:function(event,customEventData){
        
        if(customEventData.status == 4)
        {
            ComponentsUtils.showTips("暂停销售中");
        }
        else
        {
            var canvas = cc.find("Canvas");
            var chatRoomPage = cc.instantiate(this.chatRoomPrefab);
            chatRoomPage.getComponent("ChatRoomPage").initChatRoom(User.getUserCode().toString(), customEventData.lotteryCode, customEventData.roomName,customEventData.chatRoomID);
            canvas.addChild(chatRoomPage); 
        }
    },

    /** 
    * 快捷前往房间
    * @method toGotoRoom
    * @param {String} lotteryid 房间id
    */
    toGotoRoom:function(lotteryid){
       
        var lotteryid0 = parseInt(lotteryid);
        var roomInfo =  Utils.findObjByArry(this.itemListItem,"lotteryID",lotteryid0);
        if(roomInfo == null)
        {
            roomInfo =  Utils.findObjByArry(this.hotListItem,"lotteryID",lotteryid0);
        }
        if(roomInfo !=null)
        {
            var canvas = cc.find("Canvas");
            var chatRoomPage = cc.instantiate(this.chatRoomPrefab);
            chatRoomPage.getComponent("ChatRoomPage").initChatRoom(User.getUserCode().toString(), roomInfo.lotteryID.toString(), roomInfo.roomName,roomInfo.chatRoomID);
            canvas.addChild(chatRoomPage);
            chatRoomPage.getComponent("ChatRoomPage").betBtnCallback(null);
        }
        else
        {
            //cc.log("HallUI no roominfo");
        }
    },

    onDestroy:function(){
        window.Notification.offType("rLotState_push");
        window.Notification.offType("rPao_push");
    }

});

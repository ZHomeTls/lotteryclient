cc.Class({
    extends: cc.Component,

    properties: {
        userBindBankPop: {
            default: null,
            type: cc.Prefab
        },
        cardContent:{
            default:null,
            type: cc.Layout
        },

        bankItem:{
            default:null,
            type:cc.Prefab
        },

        bankcardUnbindPop:{
            default:null,
            type:cc.Prefab
        },

        addCard:{
             default:null,
            type:cc.Node
        },

        verifiedPrefab:{
            default:null,
            type:cc.Prefab
        },

        _type:0,//1:表示提现界面跳转过来2:表示银行卡管理跳转过来
        _maxBankCardNum:0,
        _currentPage:0,
        _isNowRefreshing:false,
        callback:null,
    },

    // use this for initialization
    onLoad: function () {
        this._currentPage = 1;
        this._isNowRefreshing = false;
        this.loadBankCard(this._currentPage);
    },
     
    init: function(data,callback){
        this._type = data;
        this.callback = callback;
    },

    onAddBankCard: function(){
        if(!User.getIsCertification())
        {
            var canvas = cc.find("Canvas");
            var verifiedPrefab = cc.instantiate(this.verifiedPrefab);
            canvas.addChild(verifiedPrefab);
            verifiedPrefab.active = true;
        }
        else
        {
            if(this._maxBankCardNum>0)
            {
                  ComponentsUtils.showTips("最多只能绑定1张银行卡");           
            }
            else
            {
                this.onGotoBindCard();
            }
        }
    },

    onGotoBindCard:function(){
        var canvas = cc.find("Canvas");
        var userBindBank = cc.instantiate(this.userBindBankPop);
         var Callback = function(data){
            //    cc.log("添加银行卡回调")
                this.resetLoadBankCard();
            }.bind(this);
        userBindBank.getComponent("user_bindBank_pop").init(Callback);
        canvas.addChild(userBindBank);
        userBindBank.active = true;
    },

    //滚动获取下一页
    scrollCallBack: function (scrollview, eventType, customEventData) {
        if(eventType === cc.ScrollView.EventType.BOUNCE_BOTTOM)
        {
         //   cc.log("银行卡列表：BOUNCE_BOTTOM");
            var offset_y = scrollview.getScrollOffset().y;
            var max_y = scrollview.getMaxScrollOffset().y; 
            if(offset_y - max_y>200){
                if(this._isNowRefreshing == false){
               //     cc.log("银行卡列表：下一页");
                    this._isNowRefreshing = true;
                    this.loadBankCard(this._currentPage);
                } 
            }
        }
    },

    loadBankCard:function(page){
        var recv = function(ret){
            ComponentsUtils.unblock();
            if(ret.Code != 0 && ret.Code != 49)
            {
                ComponentsUtils.showTips(ret.Msg);
                console.log(ret.Msg);
            }
            else
            {
                if(ret.Data != null)
                {
                    for(var i=0;i<ret.Data.length;i++)
                    {
                        var lastnum = ret.Data[i].CardNum.substr(ret.Data[i].CardNum.length-4);
                        var item = cc.instantiate(this.bankItem);
                        item.getComponent("user_bankCard_Item").init({
                            bankName:ret.Data[i].BankName,
                            lastNum:lastnum,
                        });
                        var clickEventHandler = new cc.Component.EventHandler();
                        clickEventHandler.target = this.node; 
                        clickEventHandler.component = "user_bankCard_pop"
                        clickEventHandler.handler = "onClickCallBack";
                        clickEventHandler.customEventData = {bankcode:ret.Data[i].BankCode,last:lastnum,bankname:ret.Data[i].BankName};
                        item.getComponent(cc.Button).clickEvents.push(clickEventHandler);
                        this.cardContent.node.addChild(item);
                        this._maxBankCardNum++;
                    }
                    this._currentPage++;
                    this.addCard.active = false;
                }
                else
                {
                    this.addCard.active = true;
                }
            }
        }.bind(this);          

        var data = {
            Token:User.getLoginToken(),
            UserCode:User.getUserCode(),
            PageNumber:page,
            RowsPerPage:11,
        }
        CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.GETBANKCARDLIST, data, recv.bind(this),"POST");     
        ComponentsUtils.block();
    },

    onClickCallBack: function(event, customEventData){
        if(this._type == 1)//选择银行卡
        {
            this.callback(customEventData);
            this.node.getComponent("Page").backAndRemove();
        }
        else if(this._type == 2)//解除绑定
        {
            var canvas = cc.find("Canvas");
            var bankcardUnbind = cc.instantiate(this.bankcardUnbindPop);
            var Callback = function(data){
              //  cc.log("解绑银行卡回调")
                this.resetLoadBankCard();
            }.bind(this);
            bankcardUnbind.getComponent("user_bankcardUnbind_pop").init(customEventData,Callback);
            canvas.addChild(bankcardUnbind);
            bankcardUnbind.active = true;
        }
    },

    //重新加载列表
    resetLoadBankCard:function(){
        this.cardContent.node.removeAllChildren();
        this._maxBankCardNum = 0;
        this._currentPage = 1;
        this.loadBankCard(1);
    },

    onClose:function(){
        if(this.callback)
           this.callback(null);
        this.node.getComponent("Page").backAndRemove();
    }

});

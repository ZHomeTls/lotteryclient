cc.Class({
    extends: cc.Component,

    properties: {
        _bEnd:false,
    },

    // use this for initialization
    onLoad: function () {
        this.pauseTime = 0;
        this.bEnd = false;
        cc.eventManager.addCustomListener(cc.game.EVENT_HIDE, function(){
            cc.log("游戏进入后台");
            this.pauseTime = new Date().getTime();
        }.bind(this));
        
        cc.eventManager.addCustomListener(cc.game.EVENT_SHOW, function(){
            cc.log("重新回到游戏!");
            var time = new Date().getTime();
            if(cc.sys.isNative){
                if(time - this.pauseTime > 300*1000){ 
                    cc.game.restart();  //重新登录的处理
                }
            }
        }.bind(this));
    }
});

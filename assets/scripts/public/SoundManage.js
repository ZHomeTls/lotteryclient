// 声音管理模块对象 
var sound_manager = {
    b_music_mute: -1, // 表示我们的背景音乐是否静音，0为没有静音，1为静音;默认为开启
    b_effect_mute: -1, // 表示我们的音效是否静音，0为没有静音，1为静音;
    
    bg_music_name: null, // 保存我们的背景音乐的文件名称的;
    bg_music_loop: false,
    clickAudio: null,

    loadAudio: function() {
        cc.loader.loadRes('audio/click', cc.AudioClip, function (err, audio) {
            if (err) {
                //cc.log('load playing audio failed');
                cc.error(err.message || err);
                return;
            }
            //cc.log('load playing audio success');
            this.clickAudio = audio;   
        }.bind(this));

    }, 

    isHasEffect:function(){
        return this.b_effect_mute;
    },

    set_music_mute: function(b_mute) {
        if(this.b_music_mute == b_mute) { // 状态没有改变;
            return;
        }
        this.b_music_mute = (b_mute) ? 1 : 0;
        
        // 如果是静音，那么我们就是将背景的音量调整到0，否则为1:
        if (this.b_music_mute === 1) { // 静音
            cc.audioEngine.stopMusic(false); // 停止背景音乐播放
        }
        else if(this.b_music_mute === 0) { // 打开
            if (this.bg_music_name) {
                cc.audioEngine.playMusic(this.bg_music_name, this.bg_music_loop)
            }  
            cc.audioEngine.setMusicVolume(1);
        }
        // 将这个参数存储到本地;
        cc.sys.localStorage.setItem("music_mute", this.b_music_mute);
    }, 
    
    set_effect_mute: function(b_mute) {
        if (this.b_effect_mute == b_mute) {
            return;
        }
        
        this.b_effect_mute = (b_mute) ? 1 : 0;

        // 将这个参数存储到本地;
        cc.sys.localStorage.setItem("effect_mute", this.b_effect_mute);
    }, 
    
    stop_music: function() {
        cc.audioEngine.stopMusic(false); // 先停止当前正在播放的;
        this.bg_music_name = null;
    }, 
    
    // 播放背景音乐
    play_music: function(file_name, loop) {
        cc.audioEngine.stopMusic(false); // 先停止当前正在播放的;
        var url = cc.url.raw(file_name);
        this.bg_music_name = url; // 保存我们当前正在播放的背景音乐;
        this.bg_music_loop = loop;
        
        if (!this.b_music_mute) {
            cc.audioEngine.playMusic(url, loop); // 当我们调用playMusic的时候，volue又回到了1;
        }
    }, 
    
    // 播放背景音效:
    play_effect: function() {
        if (this.b_effect_mute) { // 如果音效静音了，直接return;
            return;
        }
        if(this.clickAudio != null)
        {
            cc.audioEngine.play(this.clickAudio, false, 1);
        }
    },

    //播放指定音效 参数是loadRes的cc.AudioClip
    play_effect_audio: function(audio) {
        if (this.b_effect_mute) { // 如果音效静音了，直接return;
            return -1;
        }
        if(audio != null )
        {
            return cc.audioEngine.play(audio, false, 1);
        }
    },
    

    // 播放音效, 参数是url
    play_effect_url: function(url) {
        if (this.b_effect_mute) { // 如果音效静音了，直接return;
            return;
        }
        if(url != null && url != "")
            cc.audioEngine.playEffect(url);
    },
};

// 从本地获取这个用户的声音设置;
var music_mute = cc.sys.localStorage.getItem("music_mute");
if (music_mute) { // 如果本地有输出，才处理;
    music_mute = parseInt(music_mute);
}
else {
    music_mute = 0;//默认开启
}
sound_manager.set_music_mute(music_mute);

// 从本地获取这个用户的声音设置;
var effect_mute = cc.sys.localStorage.getItem("effect_mute");
if (effect_mute) { // 如果本地有输出，才处理;
    effect_mute = parseInt(effect_mute);
}
else {
    effect_mute = 0;//默认开启
}
sound_manager.set_effect_mute(effect_mute);

module.exports = sound_manager;
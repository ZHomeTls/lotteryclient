/**
 * platformutils.js
 * jsb绑定入口
 * 此脚本需要修改安卓配置及添加jsb绑定注册，暂时屏蔽，后续开放
 */

 /*
(function(){ 
    var msgUnreadArray = [];
    var msgHistoryArray = [];
    var isRecievingMsg = false;
    var enterChatRoomCallback = null;
    var purchaseCallback = null;
    var systemCallback = null;
    var loginThirdPartSDKCallback = null;  

    if(cc.sys.isNative)
    {
        if(cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID){
            var purchaseJsb = new JSB.JSBinding();
            purchaseJsb.callback = function(jsonStr){
                cc.log("jsb callback...:"+jsonStr);
                var obj = JSON.parse(jsonStr);
                cc.log("obj.functionName:"+obj.functionName);
                if(obj.functionName == "doPurchaseCallback"){
                    cc.log("doPurchaseCallback callback");
                    cc.log("doPurchaseCallback callback content:"+obj.content);
                    
                    var objForContent = null;
                    if(typeof obj.content != 'object'){ 
                        objForContent = JSON.parse(obj.content);
                    }else{
                        objForContent = obj.content;
                    }
                    purchaseCallback(objForContent.isSuccess); 
                }
            }
        }

        if(cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID){
            var systemJsb = new JSB.JSBinding();
            systemJsb.callback = function(jsonStr){
                cc.log("jsb callback...:"+jsonStr);
                var obj = JSON.parse(jsonStr);
                cc.log("obj.functionName:"+obj.functionName);
                if(obj.functionName == "doInitSystem"){
                    cc.log("doInitSystemCallback callback");
                    cc.log("doInitSystemCallback callback content:"+obj.content);
                    
                    var objForContent = null;
                    if(typeof obj.content != 'object'){ 
                        objForContent = JSON.parse(obj.content);
                    }else{
                        objForContent = obj.content;
                    }
                    systemCallback(objForContent); 
                }else if(obj.functionName == "doPickUpImageCallback"){//打开照相机
                    cc.log("doPickUpImage callback");

                    var objForContent = null;
                    if(typeof obj.content != 'object'){ 
                        objForContent = JSON.parse(obj.content);
                    }else{
                        objForContent = obj.content;
                    }
                    systemCallback(objForContent);
                } 
                else if(obj.functionName == "doTransferUserInfo")//传递android下的信息
                {
                    cc.log("doTransferUserInfo callback");
                    User.setPushTokenId(obj.content);//推送唯一ID
                    cc.log("doTransferUserInfo end");
                }
                else if(obj.functionName == "doPushMsg")//极光透传
                {
                    cc.log("doPushMsg");
                    objForContent = obj.content;
                    cc.log(objForContent);
                    var objContent = objForContent.split(',');
                    if(objContent[0] == "1")
                    {
                        window.Notification.emit("playWin",objContent);
                    }
                    else
                    {
                        cc.log("other push");
                    }

                    cc.log("doPushMsg end");
                }
            }
        }

        if(cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID){

            var loginJsb = new JSB.JSBinding();
            loginJsb.callback = function(jsonStr){
                cc.log("jsb callback...:"+jsonStr);
                var obj = JSON.parse(jsonStr);
                cc.log("obj.functionName:"+obj.functionName);
                if(obj.functionName == "doLoginCallback"){
                    cc.log("doLoginCallback callback");
                    cc.log("doLoginCallback callback content:"+obj.content);
                    
                    var objForContent = null;
                    if(typeof obj.content != 'object'){ 
                        objForContent = JSON.parse(obj.content);
                    }else{
                        objForContent = obj.content;
                    }
                    loginThirdPartSDKCallback(objForContent); 
                }  
            }
        }
    }
    
    function _doPurchase(tokenId, tradeType, callback){
        cc.log("_doPurchase in platform test");
        purchaseCallback = callback;
        
        var paramsMap = {};
        paramsMap["tokenId"] = tokenId;
        paramsMap["tradeType"] = tradeType;

        excutePlatformFun("doPurchase", paramsMap);  
    }

    function _doInitSystem(callback){
        cc.log("_doInitSystem in platform");
        systemCallback = callback;
        var paramsMap = {};

        excutePlatformFun("doInitSystem", paramsMap);
    }

    function _doLoadResDone(){
        cc.log("_doLoadResDone in platform");
        var paramsMap = {};

        excutePlatformFun("doLoadResDone", paramsMap);
    }

    function _doLoginThirdPartSDK(type, callback){
        
        cc.log("_doLoginThirdPartSDK in platform");
        loginThirdPartSDKCallback = callback;
        var paramsMap = {};
        paramsMap["type"] = type;

        excutePlatformFun("doLoginThirdPartSDK", paramsMap);
    }

    function _doShakeWithVibrator(callback){
        cc.log("_doShakeWithVibrator in platform");

        systemCallback = callback;
        var paramsMap = {};

        excutePlatformFun("doShakeWithVibrator", paramsMap);
    }

    function _doPickUpImage(callback){
        cc.log("_doPickUpImage in platform");

        systemCallback = callback;
        var paramsMap = {};

        excutePlatformFun("doPickUpImage", paramsMap);
    }

    function _copyToClipboard(content,callback){
        cc.log("_copyToClipboard in platform");
    
        systemCallback = callback;
        var paramsMap = {};
        paramsMap["content"] = content;
        excutePlatformFun("copyToClipboard", paramsMap);
    }

    function _wechatShareWithUrl(flag,url, title, dec){
        cc.log("_wechatShareWithUrl in platform");
        var paramsMap = {};
        paramsMap["flag"] = flag;
        paramsMap["url"] = url;
        paramsMap["title"] = title;
        paramsMap["dec"] = dec;

        excutePlatformFun("wechatShareWithUrl", paramsMap);
    }

    function _verUpdata(url){
        cc.log("_verUpdata in platform");
         var paramsMap = {};
         paramsMap["url"] = url;
         excutePlatformFun("verUpdata", paramsMap);
    }
 
    function excutePlatformFun(functionName, paramsMap){
        cc.log("test in sarari:"+cc.sys.os);
        cc.log("excutePlatformFun:"+functionName);
        var content = new Object(); 
        var paramsJsonStr = "";
        if(Object.keys(paramsMap).length > 0){
            for(var prop in paramsMap){
                if(paramsMap.hasOwnProperty(prop)){
                    cc.log("key is:"+prop +", and value is:"+paramsMap[prop]);
                    paramsJsonStr = JSON.stringify(paramsMap); 
                }
            }
        }
        var paramsMapAll = {};
        paramsMapAll["functionName"] = functionName;
        paramsMapAll["params"] = paramsJsonStr; 
        var jsonAll = JSON.stringify(paramsMapAll); 
    
        if(cc.sys.isNative && (cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID)){
            if(functionName == "doPurchase"){
                purchaseJsb.jsExcute(jsonAll);
            }else if(functionName == "doInitSystem" || functionName == "doLoadResDone" || functionName == "doShakeWithVibrator" || functionName == "doPickUpImage" ||
                functionName == "copyToClipboard" || functionName == "verUpdata"){
                systemJsb.jsExcute(jsonAll);
            }else if(functionName == "doLoginThirdPartSDK" || functionName == "wechatShareWithUrl"){
                loginJsb.jsExcute(jsonAll);
            }
        }else{
            
        }
    }

    var _PlatformUtils = {
		//充值支付接口 
        doPurchase:_doPurchase,
		//暂时没有用到，android传值
        doInitSystem:_doInitSystem,
        //传入第三方类型
        doLoginThirdPartSDK:_doLoginThirdPartSDK,
        //加载资源结束
        doLoadResDone:_doLoadResDone,
        //设备震动
        doShakeWithVibrator:_doShakeWithVibrator,
        //打开相机，返回照片地址
        doPickUpImage:_doPickUpImage,
        //android复制文本到剪切板
        copyToClipboard:_copyToClipboard,
        //微信分享
        wechatShareWithUrl:_wechatShareWithUrl,

        //版本更新
        verUpdata:_verUpdata,
    };
    window.PlatformUtils = _PlatformUtils;
} 
)();

*/

/**
 * !#zh 11选5走势图冷热组件 
 * @information 和值，30期，50期，100期，遗漏
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labNums:{
            default:[],
            type:cc.Label
        },
        _data:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.node.color = this._data.color;
            this.labNums[0].string = this._data.num;
            this.labNums[1].string = this._data.issus30;
            this.labNums[2].string = this._data.issue50;
            this.labNums[3].string = this._data.issue0;
            this.labNums[4].string = this._data.miss;

        }
    },

    /** 
    * 接收走势图冷热信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    },

    /** 
    * 刷新走势图冷热信息
    * @method updateData
    * @param {Object} data
    */
    updataData:function(data){
        if(data != null)
        {
            this._data = data;
            this.node.color = this._data.color;
            this.labNums[0].string = this._data.num;
            this.labNums[1].string = this._data.issus30;
            this.labNums[2].string = this._data.issue50;
            this.labNums[3].string = this._data.issue0;
            this.labNums[4].string = this._data.miss;
        }
    }

});

/**
 * !#zh 快3走势图走势组件
 * @information 期次，开奖号码，和值，大小，单双
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labIsuse:{
            default: null,
            type: cc.Label
        },
        labOpenNums:{
            default:null,
            type:cc.Label
        },
        labFormNums:{
            default:null,
            type:cc.Label
        },

        ndBg:{
            default:null,
            type:cc.Node
        },
        _data:null,
        _itemId:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.ndBg.color = this._data.color;
            var isuseStr = this._data.Isuse.substring(this._data.Isuse.length-2,this._data.Isuse.length) + "期";
            this.labIsuse.string = isuseStr;

            var openStr = "";
            for(var i=0;i<this._data.openNums.length;i++)
            {
                openStr += this._data.openNums[i]+" ";
            }
            this.labOpenNums.string = openStr;
            this.labFormNums.string = this._data.form;

        }
    },

    /** 
    * 接收走势图走势信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    },

    /** 
    * 刷新走势图走势信息
    * @method updateData
    * @param {Object} data
    */
    updateData:function(data){
        if(data != null)
        {
            this._data = data;
            var isuseStr = this._data.Isuse.substring(this._data.Isuse.length-2,this._data.Isuse.length) + "期";
            this.labIsuse.string = isuseStr;

            var openStr = "";
            for(var i=0;i<this._data.openNums.length;i++)
            {
                openStr += this._data.openNums[i]+" ";
            }
            this.labOpenNums.string = openStr;
            this.labFormNums.string = this._data.form;

        }
    },

    /** 
    * 接收走势图期号列表id
    * @method onItemIndex
    * @param {Number} i
    */
    onItemIndex: function(i){
        this._itemId =i;
    }

});
